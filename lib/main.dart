import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Aplikasi Statelesswidget Hello Word"),
        ),
        body: Center(
          child: Text("Hello Word"),
        ),
      ),
    );
  }
}